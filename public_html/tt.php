<?PHP

/* ToolTranslate PHP class
   To use on Labs, add
		require_once ( '/data/project/tooltranslate/public_html/tt.php' ) ;
		$tt = new ToolTranslation ( array ( 'tool' => 'TOOLKEY" , ... ) ) ;
*/



/*
	intitial_params keys are:
	- tool The name of the tool; the key in the online translation. Mandatory.
	- language Language to load; default is "interface_language" URL parameter, browser/system language, or 'en' as fallback. Optional.
	- fallback A fallback language, to use if a translation string is not available. If highlight_missing==true, fallback strings will be wrapped <i>in italics</i>. Optional.

	NOT IMPLEMENTED YET
	- highlight_missing Shows missing translations in the interface. true or false (default). Optional.
	- debug Show some console.log messages on problems. true or false (default). Optional.
*/
class ToolTranslation {
	public $toolname = '' ;

	protected $language = 'en' ;
	protected $fallback = '' ;
	protected $translation_cache = array() ;
	protected $language_names = array() ;
	protected $tool_info = array() ;
	protected $base_url = 'https://tools.wmflabs.org/tooltranslate/data/' ;
	protected $has_written_js = false ;
	
	public function __construct($options){
		$load_langs = array ( 'en'=>'en' ) ;
		foreach ( $options AS $k => $v ) {
			if ( $k == 'tool' ) $this->toolname = $v ;
			if ( $k == 'language' ) $this->language = $v ;
			if ( $k == 'fallback' ) $this->fallback = $v ;
			if ( $k == 'highlight_missing' ) $this->highlight_missing = $v ;
		}
		if ( $this->toolname == '' ) die ( "No toolname given" ) ;
	}
	
	public function t ( $key , $language = '' , $using_fallback = false ) {
		if ( $language == '' ) $language = $this->language ; // Default
		$this->loadLanguage ( $language ) ;
		if ( isset($this->translation_cache[$language]) and isset($this->translation_cache[$language]->$key) ) return $this->translation_cache[$language]->$key ;
		if ( !$using_fallback and $this->fallback != '' and $this->fallback != $language ) return $this->t ( $key , $this->fallback , true ) ;
		// TODO highlight_missing
		if ( $this->highlight_missing ) return "<span style='color:red'>{$this->language}:$key</span>" ;
	}
	
	public function getScriptTag () {
		return '<script type="text/javascript" src="https://tools-static.wmflabs.org/tooltranslate/tt.js"></script>' ;
	}
	
	public function getLanguage() {
		return $this->language ;
	}
	
	public function getJS ( $wrapper = '' ) {
		if ( $this->has_written_js ) return '' ;
		$this->has_written_js = true ;
		$ret = "\n<script>$(document).ready(function(){" ;
		$ret .= "tt = new ToolTranslation ( { " ;
		$ret .= "tool:'".$this->toolname."'" ;
		if ( $this->fallback != '' ) $ret .= ",fallback:'".$this->fallback."'" ;
		if ( $this->highlight_missing ) $ret .= ",highlight_missing:true" ;
		$ret .= ",callback:function(){" ;
		if ( $wrapper != '' ) $ret .= "tt.addILdropdown ( $('".$wrapper."') ) ;" ;
		$ret .= "}" ; // Callback
		$ret .= "} ) ;" ;
		$ret .= "})</script>" ;
		return $ret ;
	}
	
	protected function loadLanguageNames () {
		if ( count($this->language_names) > 0 ) return ; // Already loaded
		$this->language_names = $this->loadJSONorArray ( "languages.json" ) ;
	}
	
	protected function loadToolInfo () {
		if ( count($this->tool_info) > 0 ) return ; // Already loaded
		$this->tool_info = $this->loadJSONorArray ( $this->toolname."/toolinfo.json" ) ;
	}
	
	protected function loadLanguage ( $l ) {
		if ( isset($this->translation_cache[$l]) ) return ;
		$this->translation_cache[$l] = $this->loadJSONorArray ( $this->toolname."/$l.json" ) ;
	}
	
	protected function loadJSONorArray ( $url_part ) {
		$j = @file_get_contents ( $this->base_url . $url_part ) ;
		if ( $j === false ) return array() ; // Problem, return empty array
		return json_decode ( $j ) ;
	}
	
	
}


?>